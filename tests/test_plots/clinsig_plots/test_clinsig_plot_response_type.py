import unittest, adagenes, copy, json
import plotly
import plotly.express as px
from adagenes.plot import generate_treatment_match_type_sunburst_data
import onkopus.onkopus_clients


class TestClinSigPlotsMatchTypes(unittest.TestCase):

    def test_clinsig_plot_match_types(self):
        qid = "chr7:140753336A>T"
        # qid="chr1:114713908T>A"
        data = {copy.deepcopy(qid): {}}
        genome_version = "hg38"

        data = onkopus.onkopus_clients.UTAAdapterClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.CIViCClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.OncoKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.MetaKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.AggregatorClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.DrugOnClient(genome_version).process_data(data)

        # df = generate_treatment_sunburst_data(data, qid)
        df = generate_treatment_match_type_sunburst_data(data, qid)
        print(df.iloc[0:5, :])

        fig = px.sunburst(df, names='Drugs', path=['Biomarker', 'Response Type', 'Drug_Class', 'Drugs', 'EvLevel', 'PMID'], values='num',
                          color='Match_Type',
                          color_discrete_sequence=px.colors.qualitative.Pastel1
                          )
        # fig.update_layout(showlegend=False)
        # fig.update_coloraxes(showscale=False)

        graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

        #fig.show()


