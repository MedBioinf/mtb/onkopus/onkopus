import unittest
import adagenes.plot.generate_data.generate_radarplot_data
import adagenes.plot.generate_plots.generate_radarplot
import onkopus.onkopus_clients


class TestCategoricalVariantRadarPlot(unittest.TestCase):

    def test_multiple_variant_radar_plot(self):

        genome_version="hg38"
        variant_data = { "chr7:140753336A>T": {}, "chr1:114713908T>A": {}, "chr17:7673776G>A": {} }

        variant_data = onkopus.onkopus_clients.UTAAdapterClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.LiftOverClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.REVELClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.MVPClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.PrimateAIClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.LoFToolClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.DBNSFPClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.VUSPredictClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.AlphaMissenseClient(genome_version=genome_version).process_data(variant_data)

        dfs = adagenes.generate_data.generate_radarplot_data.generate_multiple_biomarker_radar_plot_data(variant_data)
        print(dfs)
        names=adagenes.generate_data.generate_biomarker_identifiers(variant_data)

        df_cat = adagenes.generate_data.generate_radarplot_data.generate_categorical_scores(dfs)
        print(df_cat)

        graph = adagenes.generate_plots.generate_radarplot.generate_radar_plot(df_cat,names, "pathogenicity-scores-radar")

        #print(graph)

