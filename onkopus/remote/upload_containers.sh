#!/bin/bash

. ./upload_config.ini
DEST=${DESTINATION_USER}@${DESTINATION_IP}:${DESTINATION_DIR}

echo "Upload containers in $DATA_DIR to ${DEST}"
scp -rv ./"${DATA_DIR}"/ ${DEST}
