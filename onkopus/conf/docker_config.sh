
DATE="20240417"
SHASUM=
BRANCH="main"
TARGET_DIR="./containers-${DATE}"
mkdir -v ${TARGET_DIR}

MODULES=( \
"snpservice" "clinvar-service" \
"uta-database" "uta-adapter" \
"fathmm-adapter" "fathmm-db" "m3d-adapter" "uniprot-fetcher" "vep-fetcher" "vus-predict-service" \
"revel-adapter" \
"loftool-service" \
"onkopus-server" \
"onkopus-web" \
"metakb-adapter" \
"onkopus_patlan_reverse_proxy" \
)
IMAGE_NAMES=( \
"snpservice" "clinvar-service" \
"uta-database" "utaadapter" \
"fathmmadapter" "fathmm-db" "m3dadapter" "uniprotfetcher" "vepadapter" "vuspredict" \
"revel-adapter" \
"loftool-service" \
"onkopus-server" \
"onkopus-web" \
"metakb-adapter" \
"onkopus_patlan_reverse_proxy" \
)

IMAGES=( \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/snpservice:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/clinvar-service:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/uta-database:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/utaadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/fathmmadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/fathmm-db:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/m3dadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/uniprotfetcher:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/vepadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/vuspredict:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/variant-scores/revel-adapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/variant-scores/loftool-service:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus/onkopus-server:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus/onkopus-web-frontend:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/treatment-databases/metakb-adapter:develop" \
"onkopus_vm_reverse_proxy" \
)

