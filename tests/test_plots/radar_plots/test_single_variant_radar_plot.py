import unittest
import adagenes.plot.generate_data.generate_radarplot_data
import adagenes.plot.generate_plots.generate_radarplot
import onkopus.onkopus_clients

class TestSingleVariantRadarPlot(unittest.TestCase):

    def test_single_variant_radar_plot(self):

        genome_version="hg38"
        # braf v600e
        qid = "chr7:140753336A>T"
        # nras q61l
        #qid = "chr1:114713908T>A"
        variant_data = { qid: {} }

        variant_data = onkopus.onkopus_clients.UTAAdapterClient(genome_version=genome_version).process_data(variant_data)
        #variant_data = onkopus.onkopus_clients.LiftOverClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.REVELClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.MVPClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.PrimateAIClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.LoFToolClient(genome_version=genome_version).process_data(variant_data)
        variant_data = onkopus.onkopus_clients.DBNSFPClient(genome_version=genome_version).process_data(variant_data)
        print(variant_data)

        df = adagenes.generate_data.generate_radarplot_data.generate_single_biomarker_radar_plot_data(variant_data, qid)
        names = adagenes.generate_data.generate_biomarker_identifiers(variant_data)
        dfs=[]
        dfs.append(df)
        adagenes.generate_plots.generate_radarplot.generate_radar_plot(dfs,names, "pathogenicity-scores-radar")

