#
# Preprocessing script for the Onkopus ClinVar module
#
#

# Download databases
echo "Downloading ClinVar databases..."

VERSION="20230424"

SOURCE_DIR="./data"
MODULE_DIR="clinvar"

mkdir -v "${SOURCE_DIR}/${MODULE_DIR}"
mkdir -v "${SOURCE_DIR}/${MODULE_DIR}/grch37"
mkdir -v "${SOURCE_DIR}/${MODULE_DIR}/grch38"

URL="https://ftp.ncbi.nlm.nih.gov/pub/clinvar"
DIR_GRCH37="vcf_GRCh37"
DIR_GRCH38="vcf_GRCh38"
VERSION="clinvar_${VERSION}.vcf.gz"

URL_GRCH37="${URL}/${DIR_GRCH37}/${VERSION}"
wget -v "${URL_GRCH37}" -P ${SOURCE_DIR}${MODULE_DIR}
wget -v "${URL_GRCH37}.tbi" -P ${SOURCE_DIR}${MODULE_DIR}

URL_GRCH38="${URL}/${DIR_GRCH38}/${VERSION}"
wget -v "${URL_GRCH38}" -P ${SOURCE_DIR}${MODULE_DIR}
wget -v "${URL_GRCH38}.tbi" -P ${SOURCE_DIR}${MODULE_DIR}
