import copy, json
import unittest
import plotly
import plotly.express as px
import numpy as np
'''
from adagenes.plot import generate_treatment_sunburst_data,generate_treatment_drugs_pmid_sunburst_data
import adagenes.onkopus_clients
import matplotlib.pyplot as plt


class TestClinSigPlots(unittest.TestCase):

    def test_clinsig_plots(self):
        #qid = "chr7:140753336A>T"
        qid="chr1:114713908T>A"
        data = {copy.deepcopy(qid): {}}
        genome_version="hg38"

        data = adagenes.onkopus_clients.UTAAdapterClient(genome_version).process_data(data)
        data = adagenes.onkopus_clients.CIViCClient(genome_version).process_data(data)
        data = adagenes.onkopus_clients.OncoKBClient(genome_version).process_data(data)
        data = adagenes.onkopus_clients.AggregatorClient(genome_version).process_data(data)
        data = adagenes.onkopus_clients.DrugOnClient(genome_version).process_data(data)
        #print(data)

        #df = generate_treatment_sunburst_data(data, qid)
        df = generate_treatment_drugs_pmid_sunburst_data(data, qid)

        fig = px.sunburst(df, names='EvLevel', path=['Variant', 'Cancer Type', 'EvLevel', 'Drugs'],
                          values='num',
                          color='Response Type',
                          color_continuous_scale='RdBu',
                          color_continuous_midpoint=0.00
                          )
        print(df.iloc[0:5,:])

        graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

        fig.show()
'''
