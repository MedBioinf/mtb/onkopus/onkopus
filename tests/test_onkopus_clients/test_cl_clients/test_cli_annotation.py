import unittest, os
import adagenes
import onkopus as op
import subprocess


class TestOnkopusCLIAnnotation(unittest.TestCase):

    def test_cli_annotation(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg38'
        outfile =  __location__ + '/../../test_files/somaticMutations.ann.vcf'
        op.OnkopusMain().run_interpretation("somaticMutations.ln50.vcf", outfile, mode="test",
                                            genome_version="hg19", module='all')

        with open(outfile, 'r') as file:
            read_content = file.read()

        # Check if the read content matches the expected content
        bframe = op.read_file(outfile)
        cont = "##AdaGenes v0."
        self.assertEqual(read_content[0:14], cont)
        self.assertEqual(len(list(bframe.data.keys())), 5)
