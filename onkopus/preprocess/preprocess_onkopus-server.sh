#!/bin/bash

echo "Creating Onkopus server data directory..."
mkdir -v ./data/onkopus-server

wget -v -P ../data/onkopus-server/ https://hgdownload.cse.ucsc.edu/goldenpath/hg19/liftOver/hg19ToHg38.over.chain.gz
wget -v -P ../data/onkopus-server/ https://hgdownload.cse.ucsc.edu/goldenpath/hg38/liftOver/hg38ToHg19.over.chain.gz

