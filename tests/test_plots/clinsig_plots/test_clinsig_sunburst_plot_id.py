import unittest, json
import plotly
import plotly.express as px
import adagenes, os
from adagenes.plot import generate_clinical_significance_sunburst_data_biomarker_set
import onkopus.onkopus_clients


class TestClinicalSignificanceSunburstPlotFile(unittest.TestCase):

    def test_clinical_significance_plot_file(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        #infile = "../../test_files/variants_2023-5-9.1.maf.json.clinsig"

        infile = __location__ + "/../../test_files/braf_v600e.json.clinsig"
        file_plotdata = __location__ + "/../../test_files/variants_2023-5-9.1.maf.json.clinsig.full.csv"

        data = {"chr7:140753336A>T":{}}
        bframe = adagenes.BiomarkerFrame(data=data)

        bframe.data = onkopus.onkopus_clients.UTAAdapterClient(genome_version="hg38").process_data(bframe.data)
        bframe.data = onkopus.onkopus_clients.CIViCClient(genome_version="hg38").process_data(bframe.data)

        # select variant
        #qid="chr10:105214469C>T"
        pid = "0a413f41-553d-462b-a23b-292510393567"
        variant_data = bframe.data
        #print(variant_data)

        # Generate clinical significance sunburst plot
        df = generate_clinical_significance_sunburst_data_biomarker_set(variant_data, pid)
        print(df)
        df.to_csv(file_plotdata,sep="\t")

        #fig = px.sunburst(df, names='Drugs', path=['PID','Drug_Class','Drugs','EvLevel','Biomarker','PMID'], values='num',
        #                  color='Response',
        #                  color_continuous_scale='RdBu',
        #                  color_continuous_midpoint=0.00
        #                  )
        #fig = px.sunburst(df, names='Drugs', path=['Variant','Drug_Class', 'Drugs','EvLevel','PMID'], values='num',
        #                  color='Response',
        #                  color_continuous_scale='RdBu',
        #                  color_continuous_midpoint=0.00
        #                  )
        #print(df.iloc[0:5,:])
        #graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

        #fig.show()


