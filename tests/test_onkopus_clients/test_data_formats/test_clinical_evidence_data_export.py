import unittest, os
import adagenes
import onkopus as op

class TestClinicalEvidenceDataExport(unittest.TestCase):

    def test_data_export(self):
        variant_data = { "chr7:140753336A>T":{} }
        genome_version="hg38"

        variant_data = op.LiftOverClient(genome_version).process_data(variant_data)
        variant_data = op.UTAAdapterClient(genome_version).process_data(variant_data)
        variant_data = op.ClinSigClient(genome_version).process_data(variant_data)
        bframe = adagenes.BiomarkerFrame(data=variant_data, genome_version=genome_version)

        #print(variant_data)
        #df = onkopus.tools.parse_genomic_data.get_clinical_evidence_data(variant_data)
        #print(df)
        __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
        outfile = __location__ + "/../../test_files/treatments.csv"
        op.ClinSigWriter().write_to_file(outfile, bframe)



