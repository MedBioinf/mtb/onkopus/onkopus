import unittest
import onkopus as op
import adagenes as ag
import adagenes.plot


class PathogenicityHeatmapTestCase(unittest.TestCase):

    def test_TP53_patho_heatmap(self):
        variants_protein = {"TP53:V143A":{},"TP53:R175H":{},"TP53:K132Q":{},"TP53:E285K":{},"TP53:R280K":{},"TP53:R273H":{}}

        variants = op.CCSGeneToGenomicClient(genome_version="hg38").process_data(variants_protein)
        variants = op.DBNSFPClient(genome_version="hg38").process_data(variants)

        score_labels = ["REVEL", "CADD", "AlphaMissense", "ESM1b", "EVE"]
        score_labels_raw = ["REVEL_rankscore", "CADD_raw_rankscore", "AlphaMissense_rankscore", "ESM1b_rankscore",
                            "EVE_rankscore"]

        scores = adagenes.generate_pathogenicity_plot_data(variants, score_labels_raw)

        print(scores)

        fig = ag.generate_protein_pathogenicity_plot(scores, y_title="Score", x_title="TP53",
                                                     x_labels=list(variants_protein.keys()), y_labels=score_labels, width=800, height=600, font_size=28)
        #fig.show()

