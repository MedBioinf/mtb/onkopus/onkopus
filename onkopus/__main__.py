###
# Main script for running the Onkopus biomarker interpretation framework
#
# Author: Nadine S. Kurz
#
###
import onkopus.onkopus_main

if __name__ == '__main__':
    onkopus = onkopus.onkopus_main.OnkopusMain()
    onkopus.run()


