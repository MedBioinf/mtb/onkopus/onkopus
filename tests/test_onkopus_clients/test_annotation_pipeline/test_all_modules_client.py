import unittest
import onkopus.onkopus_clients
import adagenes


class TestAllModulesClient(unittest.TestCase):

    def test_all_modules_client(self):
        """
        Tests annotation pipeline of clinical significance data
        """
        data = {"chr7:140753336A>T": {}, "chr12:25245350C>T": {}}
        #variant_dc = {"0": "chr7:140753336A>T", "1": "chr12:25245350C>T"}
        #data = {"chr7:140753336A>T": {}}
        genome_version = "hg38"

        bframe = adagenes.BiomarkerFrame()
        bframe.data=data
        #bframe = adagenes.JSONReader().read_file(infile)

        bframe.data = onkopus.onkopus_clients.AllModulesClient("hg38").process_data(bframe.data)

        print(bframe.data["chr7:140753336A>T"]["UTA_Adapter_gene"])
        self.assertEqual(bframe.data["chr7:140753336A>T"]["UTA_Adapter"]["parsed_data"],"NC_000007.14:g.140753336A>T","")
        self.assertEqual(bframe.data["chr7:140753336A>T"]["UTA_Adapter_gene"]["aromaticity_alt"], 0,"")


