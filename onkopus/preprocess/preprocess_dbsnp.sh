#
# Preprocessing script for the Onkopus dbSNP module
#
#

# Download databases

echo "Downloading dbSNP databases..."

mkdir -v ./data/dbsnp

VERSION="https://ftp.ncbi.nih.gov/snp/latest_release/VCF/GCF_000001405.40.gz"
wget -v "${VERSION}" -P data/dbsnp
wget -v "${VERSION}.md5" -P data/dbsnp
wget -v "${VERSION}.tbi" -P data/dbsnp
wget -v "${VERSION}.tbi.md5" -P data/dbsnp

# get frequency database
FREQ_DB="https://ftp.ncbi.nih.gov/snp/population_frequency/latest_release/freq.vcf.gz"
wget -v "${FREQ_DB}" -P data/dbsnp
wget -v "${FREQ_DB}.md5" -P data/dbsnp
wget -v "${FREQ_DB}.tbi" -P data/dbsnp
wget -v "${FREQ_DB}.tbi.md5" -P data/dbsnp



