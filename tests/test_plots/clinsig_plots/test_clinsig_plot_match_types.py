import unittest, adagenes, copy, json
import plotly
import plotly.express as px
import plotly.graph_objects as go
from adagenes.plot import generate_treatment_match_type_sunburst_data
import onkopus.onkopus_clients
import pandas as pd
import adagenes as ag


class TestClinSigPlotsMatchTypes(unittest.TestCase):

    def test_clinsig_plot_match_types_data(self):
        #qid = "chr7:140753336A>T"
        qid="chr12:25245351C>A"
        # qid="chr1:114713908T>A"
        data = {copy.deepcopy(qid): {}}
        genome_version = "hg38"

        data = onkopus.onkopus_clients.UTAAdapterClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.CIViCClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.OncoKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.MetaKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.AggregatorClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.DrugOnClient(genome_version).process_data(data)

        # df = generate_treatment_sunburst_data(data, qid)
        df = generate_treatment_match_type_sunburst_data(data, qid)
        pd.set_option('display.max_columns', 8)
        print(df.iloc[0:5, :])

        fig = px.sunburst(df, names='Drugs', path=['Biomarker', 'Match_Type', 'Drugs', 'EvLevel', 'PMID'], values='num',
                          color='Response Type',
                          color_continuous_scale='RdBu',
                          color_continuous_midpoint=0.00
                          )
        # fig.update_layout(showlegend=False)
        # fig.update_coloraxes(showscale=False)
        #graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

        print("columns: ", df.columns)
        print(df.head())
        print(df.shape[0])

        self.assertIsInstance(fig, go.Figure)

        self.assertEqual(df.shape[0], 497)
        self.assertEqual(df.shape[1], 13)

    def test_clinsig_plot_match_types(self):
        #qid = "chr7:140753336A>T"
        qid="chr12:25245351C>A"
        # qid="chr1:114713908T>A"
        data = {copy.deepcopy(qid): {}}
        genome_version = "hg38"

        data = onkopus.onkopus_clients.UTAAdapterClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.CIViCClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.OncoKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.MetaKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.AggregatorClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.DrugOnClient(genome_version).process_data(data)

        # df = generate_treatment_sunburst_data(data, qid)

        #fig = px.sunburst(df, names='Drugs', path=['Biomarker', 'Match_Type', 'Drugs', 'EvLevel', 'PMID'], values='num',
        #                  color='Response Type',
        #                  color_continuous_scale='RdBu',
        #                  color_continuous_midpoint=0.00
        #                  )
        # fig.update_layout(showlegend=False)
        # fig.update_coloraxes(showscale=False)
        #graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
        fig = ag.generate_sunburst_plot(data, qid,'treatment-sunburst-match-type-drugs', response_type='figure')

        self.assertIsInstance(fig, go.Figure)

