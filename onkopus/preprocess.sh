#
# Preprocessing script to preprocess and build Onkopus modules and database files
#
# Onkopus main package
#


# Download database files
mkdir -v data
cd data

DATA_URL="https://mtb.bioinf.med.uni-goettingen.de/onkopus-data"

# dbSNP


# ClinVar
mkdir -v clinvar
cd clinvar

CLINVAR_NCBI_URL_HG38="https://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz"
CLINVAR_NCBI_URL_HG37="https://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh37/clinvar.vcf.gz"

wget -v -O clinvar_hg38.vcf.gz ${CLINVAR_NCBI_URL_HG38}
wget -v -O clinvar_hg38.vcf.gz.tbi ${CLINVAR_NCBI_URL_HG38}.tbi
wget -v -O clinvar_hg37.vcf.gz ${CLINVAR_NCBI_URL_HG37}
wget -v -O clinvar_hg37.vcf.gz.tbi ${CLINVAR_NCBI_URL_HG37}.tbi
cd ..

# REVEL
mkdir -v revel
cd revel

wget -v "https://rothsj06.dmz.hpc.mssm.edu/revel-v1.3_all_chromosomes.zip"
unzip "revel-v1.3_all_chromosomes.zip"

sed 's/,/\t/g' revel_with_transcript_ids > revel_with_transcript_ids.tsv
bgzip revel_with_transcript_ids.tsv

zgrep --color -v "^[0-9XY]*[[:space:]]*[0-9]*[[:space:]]*\." revel_with_transcript_ids.tsv.gz > revel_with_transcript_ids_hg38.tsv
tail -n +2 revel_with_transcript_ids_hg38.tsv > revel_with_transcript_ids_hg38.tsv.no_header

cat revel_with_transcript_ids_hg38.tsv.no_header | sort -k1,1 -k3,3n > revel_with_transcript_ids_hg38_sorted.tsv
sed -i '1 i chr\thg19_pos\tgrch38_pos\tref\talt\taaref\taaalt\tREVEL\tEnsembl_transcriptid' revel_with_transcript_ids_hg38_sorted.tsv

bgzip -c revel_with_transcript_ids_hg38_sorted.tsv > revel_with_transcript_ids_hg38_sorted.tsv.gz
tabix -S1 -s1 -b3 -e3 ./revel_with_transcript_ids_hg38_sorted.tsv.gz
tabix -S1 -s1 -b2 -e2 ./revel_with_transcript_ids.tsv.gz
cd ..

# LoFTool

# Download all Onkopus Docker images

MODULES=( \
"snpservice" "clinvar-service" \
"uta-database" "uta-adapter" \
"fathmm-adapter" "fathmm-db" "m3d-adapter" "uniprot-fetcher" "vep-fetcher" "vus-predict-service" \
"revel-adapter" \
"loftool-service" \
"onkopus-server" \
"onkopus-web" \
"metakb-adapter" "civic-adapter" "oncokb-adapter" \
"onkopus_reverse_proxy" \
)
IMAGE_NAMES=( \
"snpservice" "clinvar-service" \
"uta-database" "utaadapter" \
"fathmmadapter" "fathmm-db" "m3dadapter" "uniprotfetcher" "vepadapter" "vuspredict" \
"revel-adapter" \
"loftool-service" \
"onkopus-server" \
"onkopus-web" \
"metakb-adapter" "civic-adapter" "oncokb-adapter" \
"onkopus_reverse_proxy" \
)
IMAGES=( \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/snpservice:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/clinvar-service:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/uta-database:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/utaadapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/fathmmadapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/fathmm-db:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/m3dadapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/uniprotfetcher:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/vepadapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/vuspredict:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/revel-adapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/loftool-service:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/onkopus-server:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/onkopus-web-frontend:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/treatment-databases/metakb-adapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/treatment-databases/civic-adapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/treatment-databases/oncokb-adapter:main" \
"https://gitlab.gwdg.de/MedBioinf/mtb/onkopus/modules/onkopus_vm_reverse_proxy:main" \
)

#for i in "${!MODULES[@]}"; do
#	docker pull ${IMAGES[i]}
#done

