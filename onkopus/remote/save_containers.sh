#!/bin/bash

# gitlab: auth saved in ~/.docker/config.json

var=$(date +'%m_%d_%Y-%H_%M_%S')

DATE="$var"
SHASUM=
BRANCH="develop"
TARGET_DIR="./containers-${DATE}"
mkdir -v ${TARGET_DIR}

MODULES=( \
"snpservice" "clinvar-service" \
"uta-database" "uta-adapter" \
"fathmm-adapter" "fathmm-db" "m3d-adapter" "uniprot-fetcher" "vep-fetcher" "vus-predict-service" \
"revel-adapter" \
"loftool-service" \
"onkopus-server" \
"onkopus-web" \
"metakb-adapter" \
"onkopus-database" \
"onkopus-websocket" \
)
IMAGE_NAMES=( \
"snpservice" "clinvar-service" \
"uta-database" "utaadapter" \
"fathmmadapter" "fathmm-db" "m3dadapter" "uniprotfetcher" "vepadapter" "vuspredict" \
"revel-adapter" \
"loftool-service" \
"onkopus-server" \
"onkopus-web" \
"metakb-adapter" \
"onkopus-database" \
"onkopus-websocket" \
)
IMAGES=( \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/snpservice:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/clinvar-service:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/uta-database:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/utaadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/fathmmadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/fathmm-db:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/m3dadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/uniprotfetcher:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/vepadapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/vus-predict/vuspredict:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/variant-scores/revel-adapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/variant-scores/loftool-service:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus/onkopus-server:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus/onkopus-web-frontend:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus-modules/treatment-databases/metakb-adapter:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus/onkopus-database:develop" \
"docker.gitlab.gwdg.de/ukeb/mtb/onkopus/onkopus-websocket-server:main" \
)

for i in "${!MODULES[@]}"; do
	docker pull ${IMAGES[i]}
	SHASUM=`docker images --digests | grep "${IMAGE_NAMES[i]}.*${BRANCH}" | grep -oP "sha256:([0-9]*[a-z])*" | grep -oP ":([0-9]*[a-z]*)*"`
	SHASUM=${SHASUM:1}
	FILENAME=${MODULES[i]}_${BRANCH}_${DATE}_${SHASUM}.tar
	echo "save ${IMAGES[i]} in ${TARGET_DIR}/${FILENAME}"
	docker save ${IMAGES[i]} > ${TARGET_DIR}/${FILENAME}
done

ln -vfns ${TARGET_DIR} "./containers"

# => copy files from VM
