import unittest
import onkopus
import onkopus.plot


class TestProteinFeaturesClass(unittest.TestCase):

    def test_protein_features_plot(self):
        variant_data = {"chr7:140753336A>T":{}}
        variant_data = onkopus.UTAAdapterClient(genome_version="hg38").process_data(variant_data)
        print(variant_data)
        for var in variant_data.keys():
            data = onkopus.plot.generate_protein_plot(variant_data[var])
        #print(data)

    def test_protein_features_plot_annotations(self):
        variant_data = {"chr7:140753336A>T": {}}
        variant_data = onkopus.UTAAdapterClient(genome_version="hg38").process_data(variant_data)
        print(variant_data)
        for var in variant_data.keys():
            data = onkopus.plot.generate_protein_plot_with_annotations(variant_data[var])
        print(data)
