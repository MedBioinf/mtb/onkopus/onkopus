
pip install seqrepo

SOURCE_DIR="./data"
MODULE_DIR="clinvar"
DEST_DIR="${SOURCE_DIR}/${MODULE_DIR}"
mkdir -v "${DEST_DIR}"

seqrepo --root-directory ${DEST_DIR} pull -i 2018-11-26

