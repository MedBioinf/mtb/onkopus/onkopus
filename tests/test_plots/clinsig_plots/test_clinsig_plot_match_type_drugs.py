import unittest, adagenes, copy, json
import plotly
import plotly.express as px
import plotly.graph_objects as go
import onkopus
from adagenes.plot import generate_treatment_match_type_sunburst_data


class TestClinSigPlotsMatchTypes(unittest.TestCase):

    def test_clinsig_plot_match_types(self):
        qid = "chr7:140753336A>T"
        # qid="chr1:114713908T>A"
        data = {copy.deepcopy(qid): {}}
        genome_version = "hg38"

        data = onkopus.onkopus_clients.UTAAdapterClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.CIViCClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.OncoKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.MetaKBClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.AggregatorClient(genome_version).process_data(data)
        data = onkopus.onkopus_clients.DrugOnClient(genome_version).process_data(data)

        # df = generate_treatment_sunburst_data(data, qid)
        df = generate_treatment_match_type_sunburst_data(data, qid)
        print(df.iloc[0:5, :])

        fig = px.sunburst(df, names='Drugs', path=['Biomarker', 'Match_Type', 'Drug_Class', 'Drugs', 'EvLevel', 'PMID'], values='num',
                          color='Response Type',
                          color_continuous_scale='RdBu',
                          color_continuous_midpoint=0.00
                          )
        # fig.update_layout(showlegend=False)
        # fig.update_coloraxes(showscale=False)

        graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

        #fig.show()
        print(df.shape)

        self.assertEqual(df.shape[0], 767)
        self.assertEqual(df.shape[1], 13)
        self.assertIsInstance(fig, go.Figure)


