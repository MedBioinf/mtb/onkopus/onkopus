import unittest, json
import plotly
import plotly.express as px
import adagenes
'''
from adagenes.plot import generate_treatment_sunburst_data,generate_treatment_drugs_pmid_sunburst_data


class TestClinicalSignificanceSunburstPlotFile(unittest.TestCase):

    def test_clinical_significance_plot_file(self):
        infile = "../../test_files/variants_2023-5-9.1.maf.json.clinsig"
        file_plotdata = "../../test_files/variants_2023-5-9.1.maf.json.clinsig.csv"

        bframe = adagenes.JSONReader().read_file(infile)
        # select variant
        qid="chr10:105214469C>T"
        variant_data = bframe.data
        #print(variant_data)

        # Generate clinical significance sunburst plot
        df = generate_treatment_drugs_pmid_sunburst_data(variant_data, qid)
        print(df)
        df.to_csv(file_plotdata,sep="\t")


        fig = px.sunburst(df, names='Drugs', path=['Variant','Drug_Class', 'Drugs','EvLevel','PMID'], values='num',
                          color='Response',
                          color_continuous_scale='RdBu',
                          color_continuous_midpoint=0.00
                          )
        ##print(df.iloc[0:5,:])
        ##graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
        fig.show()


'''
