import unittest,os
import onkopus as op
import adagenes


class TestClinSigPipeline(unittest.TestCase):

    def test_clinsig_pipeline(self):
        """
        Tests annotation pipeline of clinical significance data
        """
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        #infile = "../../test_files/variants_2023-5-9.1.maf.json"
        #outfile = "../../test_files/variants_2023-5-9.1.maf.json.clinsig"
        outfile = __location__ + "/../../test_files/braf_v600e.json.clinsig"

        data = {"chr7:140753336A>T": {}, "chr12:25245350C>T": {}}
        #variant_dc = {"0": "chr7:140753336A>T", "1": "chr12:25245350C>T"}
        #data = {"chr7:140753336A>T": {}}
        genome_version = "hg38"

        bframe = adagenes.BiomarkerFrame()
        bframe.data=data
        #bframe = adagenes.JSONReader().read_file(infile)

        bframe.data = op.ClinSigClient(genome_version="hg38").process_data(bframe.data)

        print(bframe.data)
        print(bframe.data["chr7:140753336A>T"]["onkopus_aggregator"]["clinsig_summary"])


