import unittest,os
import onkopus as op
import adagenes


class TestPathogenicityClient(unittest.TestCase):

    def test_pathogenicity_client(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))

        data = {"chr7:140753336A>T": {}, "chr12:25245350C>T": {}}
        #variant_dc = {"0": "chr7:140753336A>T", "1": "chr12:25245350C>T"}
        #data = {"chr7:140753336A>T": {}}
        genome_version = "hg38"

        bframe = adagenes.BiomarkerFrame()
        bframe.data=data
        #bframe = adagenes.JSONReader().read_file(infile)

        bframe.data = op.UTAAdapterClient("hg38").process_data(bframe.data)
        bframe.data = op.PathogenicityClient("hg38").process_data(bframe.data)

        self.assertListEqual(["variant_data","UTA_Adapter","dbnsfp", "revel", "alphamissense",  "loftool"],
                             list(bframe.data["chr7:140753336A>T"].keys()),"")


