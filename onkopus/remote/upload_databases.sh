#!/bin/bash

DB_DIR="../../data"
. ./upload_config.ini
DEST=${DESTINATION_USER}@${DESTINATION_IP}:${DB_DESTINATION_DIR}

echo "Upload database files in $DB_DIR to ${DEST}"
#scp -rv ${DB_DIR}/ ${DEST}
rsync -avrH --links ${DB_DIR} ${DEST}