#
# Preprocessing script for the Onkopus REVEL module
#
#
#!/usr/bin/bash

SOURCE_DIR="./data"
MODULE_DIR="civic"

echo "Downloading CIViC database files..."
CIVIC_URL="https://civicdb.org/downloads/01-Jul-2023"

wget "${CIVIC_URL/}/01-Jul-2023-GeneSummaries.tsv" -P ${SOURCE_DIR}${MODULE_DIR}
wget "${CIVIC_URL/}/01-Jul-2023-VariantSummaries.tsv" -P ${SOURCE_DIR}${MODULE_DIR}
wget "${CIVIC_URL/}/01-Jul-2023-MolecularProfileSummaries.tsv" -P ${SOURCE_DIR}${MODULE_DIR}
wget "${CIVIC_URL/}/01-Jul-2023-ClinicalEvidenceSummaries.tsv" -P ${SOURCE_DIR}${MODULE_DIR}
wget "${CIVIC_URL/}/01-Jul-2023-VariantGroupSummaries.tsv" -P ${SOURCE_DIR}${MODULE_DIR}
wget "${CIVIC_URL/}/01-Jul-2023-AssertionSummaries.tsv" -P ${SOURCE_DIR}${MODULE_DIR}
wget "${CIVIC_URL/}/01-Jul-2023-civic_accepted.vcf" -P ${SOURCE_DIR}${MODULE_DIR}
wget "${CIVIC_URL/}/01-Jul-2023-civic_accepted_and_submitted.vcf.tsv" -P ${SOURCE_DIR}${MODULE_DIR}
cd ${SOURCE_DIR}${MODULE_DIR}



