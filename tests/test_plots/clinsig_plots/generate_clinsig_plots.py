import os

import matplotlib.pyplot as plt

import onkopus as op
import adagenes as ag

#variant = "RET:M918T"
#variant = "BRAF:V600E"

#data = {variant:{ }}
variant = "KRAS-BRAF-TP53"
data = {"KRAS:G12V": {}, "BRAF:R462T":{},"TP53:V272A":{}}
bframe = ag.BiomarkerFrame(data=data, genome_version="hg38")
bframe.data = op.CCSGeneToGenomicClient(genome_version="hg38").process_data(bframe.data)
bframe.data = op.UTAAdapterClient(genome_version="hg38").process_data(bframe.data)
bframe.data = op.CIViCClient(genome_version=bframe.genome_version).process_data(bframe.data)
bframe.data = op.MetaKBClient(genome_version=bframe.genome_version).process_data(bframe.data)
bframe.data = op.AggregatorClient(genome_version=bframe.genome_version).process_data(bframe.data)
bframe.data = op.DrugOnClient(genome_version=bframe.genome_version).process_data(bframe.data)
bframe.data = op.InterpreterClient(genome_version=bframe.genome_version).process_data(bframe.data)
variant_data = bframe.data
#print(variant_data)

qid=""
#qid = "chr7:140753336A>T"
#plot_type="treatments-all-drug-class-drugs"


#plot_type="treatments-all-sunburst-response-type"
#plot_type="treatment-sunburst-cancer-type"
#plot_type="treatment-sunburst-match-type-drugs"
#plot_type="treatment-sunburst-match-type-drug-classes"
#plot_type="treatment-sunburst-response-type"

#type= "match-type"
#plot_type="treatments-all-sunburst-match-type-drugs-all"

type="cancer-type"
plot_type="treatments-all-cancer-type"

#type="drugs"
#plot_type="treatments-all-sunburst-match-type-drugs-all"

#type="drug_classes"
#plot_type="treatments-all-drug-class-drugs"

#type="match_types_drug_classes"
#plot_type = "treatments-all-sunburst-match-type-drug-classes-all"

# molprofile cancer type
#type="treatments-all-patient-cancer-type"
#plot_type="treatments-all-patient-cancer-type"

# mol profile drug classes
#type="treatments-all-patient-drug-class"
#plot_type="treatments-all-patient-drug-class"

# molprofile drugs
#type="treatments-all-patient-drugs"
#plot_type="treatments-all-patient-drugs"

# molprofile match type
#type="treatments-all-patient-match-types-drug-classes"
#plot_type="treatments-all-patient-match-type-drug-classes"

fig = ag.generate_sunburst_plot(variant_data, qid, plot_type, response_type="fig", font_size=32)

outfile = os.getenv("DATA_DIR") + "/" + variant + "_" + type + ".svg"
#print(outfile)
#fig.show()
fig.write_image(outfile)

